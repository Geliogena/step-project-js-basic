"use strict";


	const DATA = [
		{
			"first name": "Олексій",
			"last name": "Петров",
			photo: "./img/trainers/trainer-m1.jpg",
			specialization: "Басейн",
			category: "майстер",
			experience: "8 років",
			description:
				"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
		},
		{
			"first name": "Марина",
			"last name": "Іванова",
			photo: "./img/trainers/trainer-f1.png",
			specialization: "Тренажерний зал",
			category: "спеціаліст",
			experience: "2 роки",
			description:
				"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
		},
		{
			"first name": "Ігор",
			"last name": "Сидоренко",
			photo: "./img/trainers/trainer-m2.jpg",
			specialization: "Дитячий клуб",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
		},
		{
			"first name": "Тетяна",
			"last name": "Мороз",
			photo: "./img/trainers/trainer-f2.jpg",
			specialization: "Бійцівський клуб",
			category: "майстер",
			experience: "10 років",
			description:
				"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
		},
		{
			"first name": "Сергій",
			"last name": "Коваленко",
			photo: "./img/trainers/trainer-m3.jpg",
			specialization: "Тренажерний зал",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
		},
		{
			"first name": "Олена",
			"last name": "Лисенко",
			photo: "./img/trainers/trainer-f3.jpg",
			specialization: "Басейн",
			category: "спеціаліст",
			experience: "4 роки",
			description:
				"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
		},
		{
			"first name": "Андрій",
			"last name": "Волков",
			photo: "./img/trainers/trainer-m4.jpg",
			specialization: "Бійцівський клуб",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
		},
		{
			"first name": "Наталія",
			"last name": "Романенко",
			photo: "./img/trainers/trainer-f4.jpg",
			specialization: "Дитячий клуб",
			category: "спеціаліст",
			experience: "3 роки",
			description:
				"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
		},
		{
			"first name": "Віталій",
			"last name": "Козлов",
			photo: "./img/trainers/trainer-m5.jpg",
			specialization: "Тренажерний зал",
			category: "майстер",
			experience: "10 років",
			description:
				"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
		},
		{
			"first name": "Юлія",
			"last name": "Кравченко",
			photo: "./img/trainers/trainer-f5.jpg",
			specialization: "Басейн",
			category: "спеціаліст",
			experience: "4 роки",
			description:
				"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
		},
		{
			"first name": "Олег",
			"last name": "Мельник",
			photo: "./img/trainers/trainer-m6.jpg",
			specialization: "Бійцівський клуб",
			category: "майстер",
			experience: "12 років",
			description:
				"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
		},
		{
			"first name": "Лідія",
			"last name": "Попова",
			photo: "./img/trainers/trainer-f6.jpg",
			specialization: "Дитячий клуб",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
		},
		{
			"first name": "Роман",
			"last name": "Семенов",
			photo: "./img/trainers/trainer-m7.jpg",
			specialization: "Тренажерний зал",
			category: "спеціаліст",
			experience: "2 роки",
			description:
				"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
		},
		{
			"first name": "Анастасія",
			"last name": "Гончарова",
			photo: "./img/trainers/trainer-f7.jpg",
			specialization: "Басейн",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
		},
		{
			"first name": "Валентин",
			"last name": "Ткаченко",
			photo: "./img/trainers/trainer-m8.jpg",
			specialization: "Бійцівський клуб",
			category: "спеціаліст",
			experience: "2 роки",
			description:
				"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
		},
		{
			"first name": "Лариса",
			"last name": "Петренко",
			photo: "./img/trainers/trainer-f8.jpg",
			specialization: "Дитячий клуб",
			category: "майстер",
			experience: "7 років",
			description:
				"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
		},
		{
			"first name": "Олексій",
			"last name": "Петров",
			photo: "./img/trainers/trainer-m9.jpg",
			specialization: "Басейн",
			category: "майстер",
			experience: "11 років",
			description:
				"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
		},
		{
			"first name": "Марина",
			"last name": "Іванова",
			photo: "./img/trainers/trainer-f9.jpg",
			specialization: "Тренажерний зал",
			category: "спеціаліст",
			experience: "2 роки",
			description:
				"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
		},
		{
			"first name": "Ігор",
			"last name": "Сидоренко",
			photo: "./img/trainers/trainer-m10.jpg",
			specialization: "Дитячий клуб",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
		},
		{
			"first name": "Наталія",
			"last name": "Бондаренко",
			photo: "./img/trainers/trainer-f10.jpg",
			specialization: "Бійцівський клуб",
			category: "майстер",
			experience: "8 років",
			description:
				"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
		},
		{
			"first name": "Андрій",
			"last name": "Семенов",
			photo: "./img/trainers/trainer-m11.jpg",
			specialization: "Тренажерний зал",
			category: "інструктор",
			experience: "1 рік",
			description:
				"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
		},
		{
			"first name": "Софія",
			"last name": "Мельник",
			photo: "./img/trainers/trainer-f11.jpg",
			specialization: "Басейн",
			category: "спеціаліст",
			experience: "6 років",
			description:
				"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
		},
		{
			"first name": "Дмитро",
			"last name": "Ковальчук",
			photo: "./img/trainers/trainer-m12.png",
			specialization: "Дитячий клуб",
			category: "майстер",
			experience: "10 років",
			description:
				"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
		},
		{
			"first name": "Олена",
			"last name": "Ткаченко",
			photo: "./img/trainers/trainer-f12.jpg",
			specialization: "Бійцівський клуб",
			category: "спеціаліст",
			experience: "5 років",
			description:
				"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
		},
	];




document.addEventListener("DOMContentLoaded", ()=> {
	const ul = document.querySelector(".trainers-cards__container");
	ul.style.cssText = " left: 20%;"

	function getCards(arr) {
		const trainerTemplate = document.querySelector("#trainer-card");
		arr.forEach((el) => {
			const clone = trainerTemplate.content.cloneNode(true);
			const img = clone.querySelector(".trainer__img");
			const name = clone.querySelector(".trainer__name");
			const show = clone.querySelector(".trainer__show-more");
			img.src = el.photo;
			img.style.cssText = "min-height: 100%; z-index: 0;"
			name.textContent = el["last name"] + " " + el["first name"];
			show.style.position = "absolute";
			ul.append(clone);
		});
	}
	getCards(DATA);
function modalWind(arr, x){
	const Li = document.querySelectorAll(".trainer");

	const but = document.querySelectorAll(".trainer__show-more");
	for (let i = 0; i <= Li.length - 1; i++) {

		but[i].addEventListener("click", (e) => {
			const Temp = document.querySelector("#modal-template");
			const clone = Temp.content.cloneNode(true);
			document.body.prepend(clone);
			const modal = document.querySelector(".modal");
			modal.style.display = "none";
			const img = document.querySelector(".modal__img");
			const name = document.querySelector(".modal__name");
			const category = document.querySelector(".modal__point--category");
			const experience = document.querySelector(".modal__point--experience");
			const special = document.querySelector(".modal__point--specialization");
			const text = document.querySelector(".modal__text");
			if (e.target === but[i]) {
				modal.style.display = "block";
				img.src = arr[i]["photo"];
				name.textContent = arr[i]["last name"] + " " + arr[i]["first name"];
				category.textContent = `Категорія: ${arr[i]["category"]}`;
				experience.textContent = `Досвід: ${arr[i]["experience"]}`;
				special.textContent = `Напрям тренера: ${arr[i]["specialization"]}`;
				text.textContent = arr[i]["description"];
				document.body.style.overflow = "hidden";
			}
			const close = document.querySelector(".modal__close");
			close.addEventListener("click", () => {
				modal.style.display = "none";
				document.body.style.overflow = "scroll";
			});
		});
	}
}
	modalWind(DATA);

	//сортування
	const sectionSort = document.querySelector(".sorting");
	sectionSort.hidden = false;
	const sortButton = document.querySelectorAll(".sorting__btn");

	function sortByLastname(arr = DATA){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.sort((a, b) => {
			return a["last name"].localeCompare(b["last name"], "ua");

		});
	}
	function sortByExperience(arr = DATA){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.sort((x, y) => {
			return (y.experience[0] + y.experience[1]) - (x.experience[0] + x.experience[1]);
		});
	}
	sectionSort.addEventListener("click", (e) => {
		sortButton.forEach(function (btn) {
         if(e.target === btn){
			 btn.classList.add("sorting__btn--active");
		 }else{
			 btn.classList.remove("sorting__btn--active");
		 }
		});
	});
	sectionSort.addEventListener("click", (e)=>{
		sortButton.forEach(function (btn){
			if(e.target.innerText === "ЗА ПРІЗВИЩЕМ"){
				ul.innerHTML = "";
				getCards(sortByLastname(DATA));
				modalWind(sortByLastname(DATA));
			}else if(e.target.innerText === "ЗА ДОСВІДОМ"){
				ul.innerHTML = "";
				getCards(sortByExperience(DATA));
				modalWind(sortByExperience(DATA));
			}else{
				ul.innerHTML = "";
				getCards(DATA);
				modalWind(DATA);
			}
		});
	});



	//фільтрація
	const aside = document.querySelector(".sidebar");
	aside.hidden = false;
	aside.style.opacity = "0.8";
	const filtersBut = document.querySelector(".filters__submit");
	const inputsCategory = document.querySelectorAll("input[name='category']");
	const inputsSpecialization = document.querySelectorAll("input[name='direction']")
	function filterBySpecGym(arr){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(spec => spec.specialization === "Тренажерний зал");

	}
	function filterBySpecFight(arr){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(spec => spec.specialization === "Бійцівський клуб");
	}
	function filterBySpecKids(arr){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(spec => spec.specialization === "Дитячий клуб");
	}

	function filterBySpecSwim(arr) {
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(spec => spec.specialization === "Басейн");
	}
    console.log(filterBySpecSwim(DATA));
	function filterByCatMast(arr){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(cat => cat.category === "майстер");
	}
	function filterByCatSpec(arr){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(cat => cat.category === "спеціаліст");
	}
	function filterByCatInstr(arr){
		const copy = JSON.parse(JSON.stringify(arr));
		return copy.filter(cat => cat.category === "інструктор");
	}
const filtersInput = document.querySelectorAll(".filters__input");

	filtersBut.addEventListener("click",  (evt) => {
		evt.preventDefault()
		let filterArr = null;
               inputsCategory.forEach(function (input) {
                   if (input.checked ? input.id === "master" : false) {
					   filterArr = filterByCatMast(DATA);
				   } else if (input.checked ? input.id === "specialist" : false) {
					   filterArr = filterByCatSpec(DATA);
				   } else if (input.checked ? input.id === "instructor" : false) {
					   filterArr = filterByCatInstr(DATA);
                   } else if (input.checked ? (input.id === "all-category" || input.id === "all-direction") : false) {
					   filterArr = DATA;
                   }
               });
               inputsSpecialization.forEach(function (input) {
                   if (input.checked ? input.id === "gym" : false) {
                       ul.innerHTML = "";
                       getCards(filterBySpecGym(filterArr));
                       modalWind(filterBySpecGym(filterArr));
                   } else if (input.checked ? input.id === "fight-club" : false) {
                    ul.innerHTML = "";
                       getCards(filterBySpecFight(filterArr));
                       modalWind(filterBySpecFight(filterArr));
                   } else if (input.checked ? input.id === "kids-club" : false) {
                       ul.innerHTML = "";
                       getCards(filterBySpecKids(filterArr));
                       modalWind(filterBySpecKids(filterArr));
                   } else if (input.checked ? input.id === "swimming-pool" : false) {
                       ul.innerHTML = "";
                       getCards(filterBySpecSwim(filterArr));
                       modalWind(filterBySpecSwim(filterArr));
                   }
               });
	});
});











































